<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'clearglass_community';
$app['version'] = '2.5.46';
$app['vendor'] = 'ClearCenter';
$app['packager'] = 'ClearCenter';
$app['license'] = 'GPLv3';
$app['license_core'] = 'LGPLv3';
$app['description'] = lang('clearglass_community_app_description');
$app['powered_by'] = array(
    'vendor' => array(
        'name' => 'ClearGLASS',
        'url' => 'http://clear.glass/',
    ),
    'packages' => array(
        'clearglass-community' => array(
            'name' => 'ClearGLASS Community',
            'version' => '---',
        ),
    ),
);

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('clearglass_community_app_name');
$app['category'] = lang('base_category_server');
$app['subcategory'] = lang('base_subcategory_management');

/////////////////////////////////////////////////////////////////////////////
// Controllers
/////////////////////////////////////////////////////////////////////////////

$app['controllers']['clearglass_commnity']['title'] = $app['name'];
$app['controllers']['settings']['title'] = lang('base_settings');
$app['controllers']['containers']['title'] = lang('base_services');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['requires'] = array(
    'app-network',
    'app-certificate-manager',
    'app-clearglass',
);

$app['core_requires'] = array(
    'app-clearglass-core >= 1:2.5.20',
    'clearglass-community >= 3.3.0',
    'app-docker-core >= 1:2.5.20',
);

$app['core_file_manifest'] = array(
    'clearglass-community.php'=> array('target' => '/var/clearos/docker/project/clearglass-community.php'),
);

$app['core_directory_manifest'] = array(
    '/var/clearos/clearglass_community' => array(),
    '/var/clearos/clearglass_community/backup' => array(),
);

$app['delete_dependency'] = array(
    'app-clearglass-community-core',
    'app-clearglass-core',
    'app-clearglass',
    'clearglass-community'
);
