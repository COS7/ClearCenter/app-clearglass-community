<?php

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('clearglass_community');

///////////////////////////////////////////////////////////////////////////////
// C O N F I G L E T
///////////////////////////////////////////////////////////////////////////////

$configlet = array(
    'title' => lang('clearglass_community_app_name'),
    'app_name' => 'clearglass_community',
    'base_project' => 'clearglass',
    'container_count' => 22,
    'ignore_list' => ['elasticsearch-manage', 'apply-migrations'],
    'docker_compose_file' => '/var/lib/clearglass/docker-compose.yml',
    'images' => [
        'mongo:3.2' => lang('clearglass_image_mongo'),
        'rabbitmq:3.6.6-management' => lang('clearglass_image_rabbitmq'),
        'memcached:latest' => lang('clearglass_image_memcached'),
        'elasticsearch:5.6.10' => lang('clearglass_image_elasticsearch'),
        'registry.gitlab.com/clearglass/clearglass-community/logstash:v3-3-0' => lang('clearglass_image_logstash'),
        'registry.gitlab.com/clearglass/clearglass-community/elasticsearch-manage:v3-3-0' => lang('clearglass_image_elasticsearch_manage'),
        'kibana:5.6.10' => lang('clearglass_image_kibana'),
        'mist/docker-socat:latest' => lang('clearglass_image_docker_socat'),
        'mist/mailmock:latest' => lang('clearglass_image_mailmock'),
        'mist/swagger-ui:latest' => lang('clearglass_image_swagger_ui'),
        'registry.gitlab.com/clearglass/clearglass-community/api:v3-3-0' => lang('clearglass_image_api'),
        'registry.gitlab.com/clearglass/clearglass-community/ui:v3-3-0' => lang('clearglass_image_ui'),
        'registry.gitlab.com/clearglass/clearglass-community/landing:v3-3-0' => lang('clearglass_image_landing'),
        'registry.gitlab.com/clearglass/clearglass-community/nginx:v3-3-0' => lang('clearglass_image_nginx'),
        'influxdb:latest' => lang('clearglass_image_influxdb'),
        'registry.gitlab.com/clearglass/clearglass-community/gocky:v3-3-0' => lang('clearglass_image_gocky'),
        'traefik:v1.5' => lang('clearglass_image_traefik'),
    ]
);
