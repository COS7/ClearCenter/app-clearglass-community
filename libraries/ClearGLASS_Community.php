<?php

/**
 * ClearGLASS Community class.
 *
 * @category   apps
 * @package    clearglass-community
 * @subpackage libraries
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/clearglass-community/
 */

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\clearglass_community;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('clearglass');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\docker\Project as Project;

clearos_load_library('base/Engine');
clearos_load_library('docker/Project');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * ClearGLASS Community class.
 *
 * @category   apps
 * @package    clearglass-community
 * @subpackage libraries
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.clearcenter.com/app_license ClearCenter license
 * @link       http://www.clearcenter.com/support/documentation/clearos/clearglass-community/
 */

class ClearGLASS_Community extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * ClearGLASS constructor.
     */

    public function __construct()
    {
        clearos_profile(__METHOD__, __LINE__);
    }

    /**
     * Returns configured digital certificate.
     *
     * @return string basename of digital certificate
     * @throws Engine_Exception
     */

    public function is_installed()
    {
        clearos_profile(__METHOD__, __LINE__);

        $project = new Project('clearglass-community');

        return $project->is_installed();
    }
}
